json.extract! artwork, :id, :title, :description, :height, :width, :created_at, :updated_at
json.url artwork_url(artwork, format: :json)
