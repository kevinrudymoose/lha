# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

30.times do
  width = Faker::Number.between(100, 500)
  height = Faker::Number.between(100, 500)
  Artwork.create(title: Faker::SwordArtOnline.real_name,
                 description: Faker::Lorem.paragraph,
                 width: width,
                 height: height
                 # image: "https://via.placeholder.com/#{width}x#{height}"
  )
end