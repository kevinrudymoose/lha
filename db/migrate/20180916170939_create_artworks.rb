class CreateArtworks < ActiveRecord::Migration[5.2]
  def change
    create_table :artworks do |t|
      t.string :title
      t.text :description
      t.integer :height
      t.integer :width

      t.timestamps
    end
  end
end
